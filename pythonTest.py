#!/usr/bin/python

import csv
from bin.helper import Helper
from bin.db_query import DBRepo
import time

helper = Helper()
dbRepo = DBRepo()

APPLIED_PRICE_TYPE = 3


# def open_connection(retries, delay):                                            
#     for (x in range(0, retries)):                                            
#         conn = pymysql.connection()                                             
#         if (conn.isOpen()):                                                     
#             return conn                                                         
#         sleep(delay)                                                            
#     return None     

# conn = open_connection(30, 3)                                                   
# x = conn.cursor()        

with open("customer.csv", 'r') as csvfile:
    reader = csv.DictReader(csvfile)

    i = 0
    for row in reader:
        try:
            codeNumber  = helper.generateRandomNumber()

            # check of order number already exist, otherwise generate new
            while dbRepo.checkOrderNumberExist(codeNumber) > 1:
                codeNumber  = helper.generateRandomNumber()

            row['order_code']   = codeNumber
            row['total_amount'] = row['total']
            row['status']       = row['delivery_status']
            row['mode_of_payment'] = 3
            account             = dbRepo.getAccountByUserId(row['user_id'])

            if account is None:
                print('No account for user with user id ' + row['user_id'])
            else:
                row['account_id']   = account['id']

                # save order data
                dbRepo.insertOrderData(row)

                # get order data from order code inserted
                orders_data = dbRepo.getOrderData(row['order_code'])

                # if order data exist
                if not (orders_data is None):
                    row['order_id']   = orders_data['id']
                    
                    # inser order payment
                    dbRepo.insertPaymentData(row)

                    # insert order details
                    row['price_type'] = APPLIED_PRICE_TYPE # for customer, applied price
                    dbRepo.insertOrderDetailsData(row)

                    i += 1
            time.sleep(1)
        except Exception, e:
            print repr(e)

    
    print('TOTAL ORDERS INSERTED: ' + str(i))
csvfile.close()