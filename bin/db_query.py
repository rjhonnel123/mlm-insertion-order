#!/usr/bin/python

import sys
import pymysql 

class DBRepo:
    def __init__(self):
        self.host = "127.0.0.1"
        self.user = "root"
        self.password = "root"
        self.db = "reseller"
        self.port = 3307

    def __connect__(self):
        self.con = pymysql.connect(host=self.host, user=self.user, password=self.password, db=self.db, port=self.port, cursorclass=pymysql.cursors.
                                   DictCursor)
        
        self.cur = self.con.cursor()

    # def open_connection(retries, delay):                                            
    #     for (x in range(0, retries)):                                            
    #         conn = self.__connect__()                                            
    #         if (conn.isOpen()):                                                     
    #             return conn                                                         
    #         sleep(delay)                                                            
    #     return None    

    def __disconnect__(self):
        self.con.close()

    def insertOrderData(self, rowData):
        try:
            self.__connect__()
            sqlInsert = """INSERT INTO orders (code , account_id, status, total_revenue)  
                                     VALUES ('{order_code}', {account_id}, {status}, {total}) """

            sql = sqlInsert.format(order_code = rowData['order_code'],
                                                 account_id = rowData["account_id"],
                                                 status = rowData['status'],
                                                 total = rowData['total_amount'])
            self.cur.execute(sql)
            self.con.commit()
            print("Inserted order " + rowData['order_code'])
            self.__disconnect__()
        except Exception as e:
            sys.exit(1)
        finally:
            self.cur.close()

    def checkOrderNumberExist(self, code):
        try:    
            self.__connect__()                           
            selectQuery = "SELECT id from orders where code ='{code}'"

            sql = selectQuery.format(code = code)

            self.cur.execute(sql)
            result = self.cur.rowcount
            self.__disconnect__()

            return result
        except Exception as e:
            sys.exit(1)
        finally:
            self.cur.close()
        

    def getOrderData(self, code):
        try:        
            self.__connect__()                           
            selectQuery = "SELECT * from orders where code ='{code}'"

            sql = selectQuery.format(code = code)

            self.cur.execute(sql)
            result =  self.cur.fetchone()
            self.__disconnect__()

            return result
        except Exception as e:
            sys.exit(1)
        finally:
            self.cur.close()
            
    def getAccountByUserId(self, user_id):
        try:
            self.__connect__()                         
            selectQuery = "SELECT * from accounts where user_id ='{user_id}'"

            sql = selectQuery.format(user_id = user_id)

            self.cur.execute(sql)
            result = self.cur.fetchone()
            self.__disconnect__()

            return result
        except Exception as e:
            sys.exit(1)
        finally:
            self.cur.close()

    def insertPaymentData(self, rowData):
        try:
            self.__connect__()
            sqlInsert = """INSERT INTO order_payments (payment_method_type , order_id, payment_amount)  
                                 VALUES ('{payment_method_type}', {order_id}, {payment_amount}) """

            sql = sqlInsert.format(payment_method_type = rowData['mode_of_payment'],
                                             order_id = rowData['order_id'],
                                                 payment_amount = rowData['total_amount'])
            self.cur.execute(sql)
            self.con.commit()

            print("Inserted order payment for order " + rowData['order_code'])
            self.__disconnect__()
        except Exception as e:
            sys.exit(1)
        finally:
            self.cur.close()

    def insertOrderDetailsData(self, rowData):
        try:
            self.__connect__()
            sqlInsert = """INSERT INTO order_details (order_id, quantity, total_revenue, applied_product_price, product_id, price_type)  
                                 VALUES ('{order_id}', {quantity}, {total_revenue}, {applied_product_price}, {product_id}, {price_type}) """

            sql = sqlInsert.format(order_id = rowData['order_id'],
                                                 quantity = rowData['quantity'],
                                                 total_revenue = rowData['total_amount'],
                                                 applied_product_price = rowData['product_price'],
                                                 product_id = rowData['product_id'],
                                                 price_type = rowData['price_type']
                                                 )
            self.cur.execute(sql)
            self.con.commit()

            print("Inserted order details for order " + rowData['order_code'])
            self.__disconnect__()
        except Exception as e:
            sys.exit(1)
        finally:
            self.cur.close()

