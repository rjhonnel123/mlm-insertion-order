#!/usr/bin/python

import pymysql 

def sql_db_connect():
    connection = pymysql.connect(host="localhost", db="mlm08",
            user="root", passwd="root",
            port=3306, cursorclass=pymysql.cursors.DictCursor)
    return connection
